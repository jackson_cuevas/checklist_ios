//
//  Checklist.swift
//  Checklist
//
//  Created by Wepsys_dev on 7/15/19.
//  Copyright © 2019 hacksonLab. All rights reserved.
//

import Foundation

class ChecklistItem {
    var text = ""
    var checked = false
    
    func toggleChecked() {
        checked = !checked
    }
}
